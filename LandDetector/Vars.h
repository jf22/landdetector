//static volatile int16_t heading;
int system_id = 99;                            //systhem ID of this system              
//int component_id = MAV_COMP_ID_PATHPLANNER;  //who is sending the information?
int component_id = MAV_COMP_ID_PERIPHERAL;  //who is sending the information?
uint8_t system_type = MAV_TYPE_CAMERA;         // define the system type, in this case an camera
uint8_t autopilot_type = MAV_AUTOPILOT_INVALID; // 8
uint8_t system_mode   = 0; 
uint32_t custom_mode  = 0;

//System states
uint8_t system_state  = MAV_STATE_STANDBY; // 4 = MAV_STATE_ACTIVE	System is active and might be already airborne. Motors are engaged.
 /*
  MAV_STATE_STANDBY System is grounded and on standby. It can be launched any time.
  MAV_STATE_ACTIVE  System is active and might be already airborne. Motors are engaged.
  MAV_STATE_CRITICAL  System is in a non-normal flight mode. It can however still navigate.
  MAV_STATE_EMERGENCY System is in a non-normal flight mode. It lost control over parts or over the whole airframe. It is in mayday and going down.
  MAV_STATE_POWEROFF  System just initialized its power-down sequence, will shut down now.
  MAV_STATE_FLIGHT_TERMINATION
 */

uint8_t base_mode     = 1; // < The new base mode
uint8_t target_system = 1;
uint8_t target_component = 0;
int   zacc=0;
//int   zgyro=0;
long weight;
long weight2;
long weight3;
long weight4;
long weightMean;
bool __debugSerialTwoToConsoleOne = false;

bool rebootDone;

bool __SendHB = false;
bool __InjectRangerMav = false;

bool armed;
uint32_t ultraslowLoop;
uint32_t slowLoop;
uint32_t midLoop;
uint32_t fastLoop;
uint32_t debugLoop;
uint32_t plumbLoop;
uint32_t loopMeasure;
uint32_t storeAccTime;
uint32_t storeBamTime;
uint16_t loopTime;

uint8_t __MAIN_STATE = 0;
uint8_t __LAST_MAIN_STATE = 255;
// Mavlink variables
uint32_t previousMillisMAVLink = 0;     // will store last time MAVLink was transmitted and listened
uint8_t system_status;
float __distanceSensor;
float __olddistanceSensor;
uint8_t currentmode;
uint8_t oldcurrendmode;
int Rc10;
bool __EXT_STATE_INTERRUPT = false; // Das kann alles sein Zb. Sinkrate zu hoch dann wird das Landegestell Abgeschaltet und erst nach dem nächsten arming wieder scharf



// Rangefinder and EulerRotationMAtrix Global variables

uint16_t tfMiniDist = 0;       // Distance measurement in centimeters (default)
uint16_t tfMiniFlux = 0;       // Luminous flux or intensity of return signal
uint16_t tfMiniTemp = 0;       // Temperature in degrees Centigrade (coded)
uint16_t tfDist = 0;
uint16_t tfDist_used = 0;
uint16_t tfFlux = 0;
uint16_t tfTemp = 0;

int __accx = 0;
int __accy = 0;
int __accz = 0;
int __gyrox = 0;
int __gyroy = 0;
int __gyroz = 0;

float __attitudePitchDeg = 0.0f;
float __attitudeRollDeg = 0.0f;
float __attitudePitchRad = 0.0f;
float __attitudeRollRad = 0.0f;
float __attitudeAngleDegSum = 0.0f;
float __rangefinderPlumb = 0.0f;
float __lowGearPlumb = 0.0f;
int8_t __lowGearCorner = 0;

float __sinkRateAVRLowLevel = 0.0f;
float __sinkRateAverage = 0.0f;
float __sinkRate = 0.0f;
float __oldSinkRatePlumb = 0.0f;
uint32_t __oldSinkrateTime = 0;

int32_t __mavlinkRawAltInt = 0.0f;
int32_t __mavlinkRawGpsAlt = 0;
int32_t __mavlinkGPSPosLat = 0;
int32_t __mavlinkGPSPosLon = 0;
uint64_t __mavlinkGPSTime = 0;

int __stateError = 0;

unsigned long __systemTimeMs = 0;



//High Latency

int16_t temperature;

#ifdef setParamEnable
// Parameter
 float param_value; /*<  Onboard parameter value*/
 char param_id[16]; /*<  Onboard parameter id, terminated by NULL if the length is less than 16 human-readable chars and WITHOUT null termination (NULL) byte if the length is exactly 16 chars - applications have to provide 16+1 bytes storage if the ID is stored as string*/
 uint8_t param_type; /*<  Onboard parameter type.*/
 #endif //setParamEnable
 //char msgbuf;
 bool taredone;
 bool logDelay;
 uint16_t logDelayTimer;
 bool accPeak;
 bool _bam;
 //functions

// coords +X point to front, +Y point to right, +Z point to top

struct LanderConfig {
  float IMU_Z;
  float RAN_X;
  float RAN_Y;
  float RAN_Z;
  float PONE_X;
  float PONE_Y;
  float PTWO_X;
  float PTWO_Y;  
  float PTHREE_X;
  float PTHREE_Y;
  float PFOUR_X;
  float PFOUR_Y;
  float MINGEARTOGROUNDDISTANCE_Z;
  float MINACTIVATIONALT_Z;
  int DMSMINWEIGHT;
  int MINACC;
  int STOREACCPEAKTIME;
  String VERSION;
  float FREEONE;  // DSM SCALE 1
  float FREETWO;  // DSM SCALE 2
  float FREETHREE;// DSM SCALE 3
  float FREEFOUR; // DSM SCALE 4
  float FREEFIVE; // Config Enable disable HB 0.0 = off, 1.0 = on
  float FREESIX; // Config Enable Disable Rangfinder Mav Inject 0.0 = off, 1.0 = on
  float FREESEVEN;
  float FREEEIGHT;
  float FREENINE;
  float FREETEN;
};

  LanderConfig currentConfig = {
    40.0f,
    0.0f,
    -21.0f,
    35.0f,
    30.0f,
    30.0f,
    -30.0f,
    30.0f,
    -30.0f,
    -30.0f,
    30.0f,
    -30.0f,
    10.0f,
    80.0f,
    -30,
    -1500,
    300,
    "R0.9",
    100.0f,
    100.0f,
    100.0f,
    100.0f,
    1.0f,// enable disable heartbeat
    1.0f,// enable disable inject rangefinder
    7.0f,
    8.0f,
    9.0f,
    10.0f
  };


  // Lander Errror Struct
  // | Int Error | Int State | GPS Time | GPS Pos Lat | GPS Pos Lon | Ranger AltitudePlum | Baro Altitude | GPS ALT | RC 10
  int _nextLogAddress = 150;
  uint16_t _nextLogID = 1;
  
  struct landerError{
    char VALID;
    uint16_t ID;
    int16_t ERROR;
    int16_t STATE;
    uint64_t GPS_TIME;
    int32_t GPS_LAT;
    int32_t GPS_LON;
    float RANGEFINDERPLUMB;
    float MAVLINKBAROALT;
    int32_t MAVLINKGPSALT;
    int16_t SCALEONE;
    int16_t SCALETWO;
    int16_t SCALETHREE;
    int16_t SCALEFOUR;
    char CORNER;
    int16_t RCTEN;
  };

// init error Struct

  landerError myErrors{
    0, //char valid, if 112 we hold data
    0, //int id
    0, //int Eerror
    0, //int State
    99999999999, //uint64_t GPS Time
    0, //int32_t Gps Lat
    0, //int32_t Gps Lon
    0.0f, //float Rangfinder Plumb
    0.0f, //float Baro Alt cm
    0, // int Gps Alt mm
    0, // int Scale 1
    0, // int Scale 2
    0, // int Scale 3
    0, // Int Scale 4
    0, // char corner
    0 // int RC 10 Pwm
  };

  landerError myErrorsTest{
    0, //char valid, if 112 we hold data
    0, //int id
    0, //int Eerror
    0, //int State
    99999999999, //uint64_t GPS Time
    0, //int32_t Gps Lat
    0, //int32_t Gps Lon
    0.0f, //float Rangfinder Plumb
    0.0f, //float Baro Alt cm
    0, // int Gps Alt mm
    0, // int Scale 1
    0, // int Scale 2
    0, // int Scale 3
    0, // Int Scale 4
    0, // char corner
    0 // int RC 10 Pwm
  };

    landerError myErrorsClear{
    0, //char valid, if 112 we hold data
    0, //int id
    0, //int Eerror
    0, //int State
    99999999999, //uint64_t GPS Time
    0, //int32_t Gps Lat
    0, //int32_t Gps Lon
    0.0f, //float Rangfinder Plumb
    0.0f, //float Baro Alt cm
    0, // int Gps Alt mm
    0, // int Scale 1
    0, // int Scale 2
    0, // int Scale 3
    0, // Int Scale 4
    0, // char corner  
    0 // int RC 10 Pwm
  };
