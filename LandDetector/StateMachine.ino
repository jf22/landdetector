/**
 * Statemachine 
 * 
 * "__MAIN_STATE" States:
 * 
 *  State Value |     State Name                    |     Transition
 * -------------|-----------------------------------|--------------------------------- 
 *            0 | LG_INIT_STG                       | Init LandinggearState Fix
 *            1 | LG_COPTER_AMED_STG                | from disarmed copter TO armed copter
 *            2 | LG_COPTER_REACH_MIN_ACTIVATION_ALT| Takeoff to minActivationAlt
 * ------------------------------------------------------------------------------------
 * #############|######## AUTO Modes logic #########|#################################
 *           10 | LG_COPTER_AUTO....?               | Mission Stages to to find out...
 *           70 | LG_COPTER_AUTO....?               | ... Mission things free space
 *           71 | LG_COPTER_R_AND_BARALT_ON_6M_OK   | Check Baro Altitude at 6m Altitude against Rangefinder
 *           72 | LG_COPTER_R_AND_BARALT_ON_3M_OK   | Check Baro Altitude at 3m Altitude against Rangefinder
 * #############|##### END AUTO Modes logic ########|################################# 
 * ------------------------------------------------------------------------------------
 * #############|###### REAL SENSOR LAND LOGIC #####|#################################
 *           80 | LG_COPTER_ARM_GEAR                | in AP modes != Auto we reach this after takeoff to 1M
 *           90 | LG_COPTER_RANGER_LOW_10CM         | RANGEFINDER PLUMB < 10 cm
 *           91 | LG_COPTER_ACCZ_HIGH               | ACC Z high
 *           92 | LG_GEAR_SCALE_TRUE                | ONE SCALE < 20
 *          100 | LG_COPTER_IS_LANDED               | after ONE SCALE < 20, - or if SCALEs disabled after ACC Z high  
 * 
 */

// System Error/Info List
#define ERROR_RANGEFINDER_HIGH_SINKRATE_UNDER_MIN_HIGHT 101
#define ERROR_RANGEFINDER_HIGH_SINKRATE_ABOVE_MIN_HIGHT 102
#define INFO_RANGEFINDER_FIRST_ACCZ_HIGH_ABOVE_MIN_HIGHT 10
#define INFO_RANGEFINDER_SECOND_ACCZ_HIGH_ABOVE_MIN_HIGHT 11
#define INFO_RANGEFINDER_THIRD_ACCZ_HIGH_ABOVE_MIN_HIGHT 12
// Inflight problems
uint8_t acczWasHight = 0;

// Error protokol
/**
 *
 *  Int Error | Int State | GPS Time | GPS Pos Lat | GPS Pos Lon | Ranger AltitudePlum  | Baro Altitude | GPS ALT | RC 10
 * 
 */ 



// defines Copter States
#define LG_INIT_STG 0
#define LG_COPTER_AMED_STG 1
#define LG_COPTER_REACH_MIN_ACTIVATION_ALT 2
#define LG_COPTER_R_AND_BARALT_ON_6M_OK 71
#define LG_COPTER_R_AND_BARALT_ON_3M_OK 72
#define LG_COPTER_ARM_GEAR 80
#define LG_COPTER_RANGER_LOW_10CM 90
#define LG_COPTER_ACCZ_HIGH 91
#define LG_GEAR_SCALE_TRUE 92
#define LG_COPTER_IS_LANDED 100

//variable of EXT_STATE_INTERRUPTS
int8_t EXT_STATE_INTERRUPTS_RC = -1; // -1 = OFF = <1400pwm | 0 = Gear Full > 1400 && < 1600| 1 = Gear without SCALES > 1600
int8_t LAST_EXT_STATE_INTERRUPTS_RC = -10;
bool EXT_STATE_INTERRUPTS_RC_CHANGE = false;
bool resetMainStage = false;
uint32_t landingTime = 0;

bool tareScales = false;


// Compute new State
void simpleFsmComputeState(){
  // Check RC CHANNEL
  LAST_EXT_STATE_INTERRUPTS_RC = EXT_STATE_INTERRUPTS_RC;

  //achtung - dass muß <2 sein!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  if(currentmode<2) EXT_STATE_INTERRUPTS_RC = -1; // 0 = stabelise, 1 = acro
  else EXT_STATE_INTERRUPTS_RC = 0;
  #ifdef use_rc_over_mavlink_input
  if(Rc10 > 1600) EXT_STATE_INTERRUPTS_RC = 1;                // On without Loadcell // 1 if we test witout loadcell
  if(Rc10 < 1600 && Rc10 > 1400) EXT_STATE_INTERRUPTS_RC = 0; // Full ON
  if(Rc10 < 1400) EXT_STATE_INTERRUPTS_RC = -1;               // Off
  #endif

  //Serial.println(EXT_STATE_INTERRUPTS_RC);
  
  if(EXT_STATE_INTERRUPTS_RC != LAST_EXT_STATE_INTERRUPTS_RC){
    EXT_STATE_INTERRUPTS_RC_CHANGE = true;
    //Serial.printf("RC Switch: %d Bool Change:\n", EXT_STATE_INTERRUPTS_RC,EXT_STATE_INTERRUPTS_RC_CHANGE );
  }
  if(EXT_STATE_INTERRUPTS_RC == LAST_EXT_STATE_INTERRUPTS_RC){
    EXT_STATE_INTERRUPTS_RC_CHANGE = false;
    //Serial.println(EXT_STATE_INTERRUPTS_RC_CHANGE);
  }
  // If __EXT_STATE_INTERRUPT is true because an Birdstrike we can reactivate the gear by switching the gear off on
  // !!!!!! HAHLA TEST we deactivate this. An Error happens One Time
   if(__EXT_STATE_INTERRUPT && EXT_STATE_INTERRUPTS_RC == -1){
    __EXT_STATE_INTERRUPT = false;
    __stateError = 0;
  } 
    
  
  // Transition To Arming The Copter
  if(__MAIN_STATE < LG_COPTER_AMED_STG && armed){// THIS happens after Powering the System or if we already truly Landed
    __MAIN_STATE = LG_COPTER_AMED_STG;
    __EXT_STATE_INTERRUPT = false;
    __stateError = 0;
    acczWasHight = 0;
    system_state = MAV_STATE_STANDBY;
  }
  if(!armed){
    __MAIN_STATE = LG_INIT_STG;
    //mavlink system state
    system_state = MAV_STATE_STANDBY; 
  }



  // ############# DEBUG COPTER WITHOUT real ARMING ################
  //if(__MAIN_STATE == 0) __MAIN_STATE =1;
  
  //Takeoff 1m
  if(__MAIN_STATE >= LG_COPTER_AMED_STG && __lowGearPlumb >= currentConfig.MINACTIVATIONALT_Z){
    if(__MAIN_STATE < LG_COPTER_REACH_MIN_ACTIVATION_ALT){
      __MAIN_STATE = LG_COPTER_REACH_MIN_ACTIVATION_ALT;
      tareScales = true;
    }
    system_state = MAV_STATE_ACTIVE; 
  }
  
  // Check if we not in Automode or guided mode
  // 4 = Guided, 20 = Guided no GPS, 3 = AutoMode
  // ############### ! All Guide and Auto Modes currently not implemented##################
  if(__MAIN_STATE >= LG_COPTER_REACH_MIN_ACTIVATION_ALT && (currentmode != 3 || currentmode != 4 || currentmode != 20) ){
    // Ignore all automode steps
    __MAIN_STATE = LG_COPTER_ARM_GEAR;
  }
  if (__MAIN_STATE >= LG_COPTER_REACH_MIN_ACTIVATION_ALT && (currentmode == 3 || currentmode == 4 || currentmode == 20) ){
    // ### define auto mode steps, currently not implemented ###########
    // we disable the landinggear here
    // first we also enable gear at these point to try if this difference is important
    
    //__MAIN_STATE = LG_INIT_STG;
    __MAIN_STATE = LG_COPTER_ARM_GEAR;
    system_state = MAV_STATE_ACTIVE;
  }
  
  // ############ MACHINE GO ####################

  // Check inflight ACCZ Peaks
  if(__MAIN_STATE >= LG_COPTER_ARM_GEAR && acczWasHight <=3 && accPeak == true){
    if(acczWasHight == 0){
      acczWasHight++;
      //handleErrorLog(__MAIN_STATE, 10);
    }
    if(acczWasHight == 1){
      acczWasHight++;
      //handleErrorLog(__MAIN_STATE, 11);
    }
    if(acczWasHight == 2){
      acczWasHight++;
      //handleErrorLog(__MAIN_STATE, 12);
    } 
  }

  //Check Rangefinder < 10 cm transition to next Stage check acc
  // We only accept < 10cm groundlevel if sinkRate < abs500
  if(__MAIN_STATE >= LG_COPTER_ARM_GEAR && __lowGearPlumb < currentConfig.MINGEARTOGROUNDDISTANCE_Z && __sinkRateAverage > -200.0 ){
    __MAIN_STATE = LG_COPTER_RANGER_LOW_10CM;
  }
  if(!__EXT_STATE_INTERRUPT && __MAIN_STATE >= LG_COPTER_RANGER_LOW_10CM && __lowGearPlumb < currentConfig.MINACTIVATIONALT_Z && __sinkRateAverage < -170.0){
    Serial.println("Vogelflug unter mindesthöhe :(( disable gear now");
    __EXT_STATE_INTERRUPT = true;
    __stateError = ERROR_RANGEFINDER_HIGH_SINKRATE_UNDER_MIN_HIGHT;
    handleErrorLog(__MAIN_STATE, __stateError);
  }
  if(!__EXT_STATE_INTERRUPT && __MAIN_STATE >= LG_COPTER_ARM_GEAR && __sinkRateAverage < -250.0){
    Serial.println("Vogelflug uber mindesthöhe :(( disable gear now");
    __EXT_STATE_INTERRUPT = true;
    __stateError = ERROR_RANGEFINDER_HIGH_SINKRATE_ABOVE_MIN_HIGHT;
    handleErrorLog(__MAIN_STATE, __stateError);
  }

  //Check Rangefinder back
  if(__MAIN_STATE >= LG_COPTER_RANGER_LOW_10CM && __lowGearPlumb > currentConfig.MINGEARTOGROUNDDISTANCE_Z){
    __MAIN_STATE = LG_COPTER_ARM_GEAR;
  }

  // Check ACCZ if 
 
  if( __MAIN_STATE >= LG_COPTER_RANGER_LOW_10CM && accPeak==true){
    __MAIN_STATE = LG_COPTER_ACCZ_HIGH;

  }
  // Hier könnte mit RC3 eingang > 1600 die Landung abgebrochen werden
  
  
  // SCALES 
  if( __MAIN_STATE >= LG_COPTER_ACCZ_HIGH && (weight<currentConfig.DMSMINWEIGHT||weight2<currentConfig.DMSMINWEIGHT||
                                              weight3<currentConfig.DMSMINWEIGHT||weight4<currentConfig.DMSMINWEIGHT)){    
    __MAIN_STATE =  LG_GEAR_SCALE_TRUE;
  }

  // Finale check RC 0 || -1 Center With Scale
  if( EXT_STATE_INTERRUPTS_RC == -1 || EXT_STATE_INTERRUPTS_RC == 0 ){
    if(!__EXT_STATE_INTERRUPT && __MAIN_STATE >= LG_GEAR_SCALE_TRUE) __MAIN_STATE = LG_COPTER_IS_LANDED;
  }
  // Final Check RC 1 Stick up Without Scale
  if( EXT_STATE_INTERRUPTS_RC == 1){
    if(!__EXT_STATE_INTERRUPT && __MAIN_STATE >= LG_COPTER_ACCZ_HIGH) __MAIN_STATE = LG_COPTER_IS_LANDED;
  }  

  #ifdef debugStateMAchine
  if(__LAST_MAIN_STATE != __MAIN_STATE || oldcurrendmode != currentmode )
    Serial.printf("State: %d, FlightMode %d, EXT_INTERRUPT %d\n",__MAIN_STATE, currentmode,__EXT_STATE_INTERRUPT);
    oldcurrendmode = currentmode;
  #endif
  __LAST_MAIN_STATE = __MAIN_STATE;
  

  // After New States Computed we call Stage Handler 
  simpleFsmStateHandler();


  // set mavlink systemstate on error high
  if(__EXT_STATE_INTERRUPT){
    system_state = MAV_STATE_CRITICAL;
//    if(!armed){// recover to standby if comper is disarmed after fly
//      system_state = MAV_STATE_STANDBY;
//    }
  }
}


void simpleFsmStateHandler(){
  switch (__MAIN_STATE) {
    case LG_INIT_STG:
      //Nothing to do, Copter and Landinggear are just Powered on
    break;

    case LG_COPTER_IS_LANDED: // LAST STAGE LANDINGGEAR TRUE TOCHDOWN
      if(!resetMainStage && EXT_STATE_INTERRUPTS_RC > -1) arming(false);
      resetMainStage = true;
      landingTime = millis();
    break;

    default:
      if(tareScales){
        #ifdef scaleEnable
        Serial.println("TARE SCALES in Flight");
        scales.tare(3, 100000);
        Serial.println("TARE SCALES in Flight --- ready");
        #endif
        tareScales = false;
      }
    break;
  }

  //reset mainstage after 100ms
  if(resetMainStage && millis() - 100 > landingTime){
     __MAIN_STATE = LG_INIT_STG; // TSAGEMACHINE back to First State
     resetMainStage = false;
  }
}

// EEprom Adresses
// 1-2 Initstate,
// 3-126 Configuration
// 127-149 free for extra config options
// 150 - 2048 free for logging
// error log need 50 byte
// 150, 200, 250 .......
void handleErrorLog(int state, int error){
    Serial.printf("Error Detected! State: %d, Error: %d\n",state,error);
    myErrors.ID = _nextLogID;
    myErrors.VALID = 112;

    myErrors.STATE = state;
    myErrors.ERROR = error;

    myErrors.GPS_TIME = __mavlinkGPSTime;
    myErrors.GPS_LAT = __mavlinkGPSPosLat;
    myErrors.GPS_LON = __mavlinkGPSPosLon;
    myErrors.RANGEFINDERPLUMB = __rangefinderPlumb;
    myErrors.MAVLINKBAROALT = __mavlinkRawAltInt;
    myErrors.MAVLINKGPSALT = __mavlinkRawGpsAlt;
    myErrors.SCALEONE = weight3;
    myErrors.SCALETWO = weight4;
    myErrors.SCALETHREE = weight;
    myErrors.SCALEFOUR = weight2;
    myErrors.CORNER = __lowGearCorner;
    myErrors.RCTEN = Rc10;

    EEPROM.put(_nextLogAddress, myErrors);
    if(_nextLogAddress >=1950){// nextlog is first
       _nextLogAddress = 150;
       EEPROM.get(150,myErrorsClear);
       myErrorsClear.VALID = 0;
       EEPROM.put(150,myErrorsClear);
    }
    else
    {
       _nextLogAddress = _nextLogAddress + 50;
       EEPROM.get(_nextLogAddress, myErrorsClear);
       myErrorsClear.VALID = 0;
       EEPROM.put(_nextLogAddress, myErrorsClear);
    }
    _nextLogID++;
}
